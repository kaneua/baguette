// preprocessor.cs
//
// File that contains classes for preprocessing program source files before
// passing them to parser. It isn't intended to be fast at least because it will
// run only once per compilation.

using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;

static class Preprocessor
{
	private class Symbol
	{
		public int Index{get; set;} // Position in string
		public int Size{get; set;} // Size of symbol in characters
		public string Type{get; set;} // Type of symbol

		public Symbol(int index, int size, string type)
		{
			Index = index;
			Size = size;
			Type = type;
		}
	}

	private class SymbolComparer: IComparer<Symbol>
	{
		public int Compare(Symbol x, Symbol y)
		{
			if(x.Index > y.Index)
			{
				return 1;
			}

			if(x.Index < y.Index)
			{
				return -1;
			}

			if(x.Index == y.Index)
			{
				return 0;
			}

			return 0;
		}
	}

	static List<string> rawInput = new List<string>();
	static List<string> output = new List<string>();

	//Regular expressions for parsing
	static string newMethodRegex = @"(void|string|int|float|bool)\s+[a-zA-Z_][a-zA-Z0-9_]*\s*\x28";
	static string ifRegex = @"if\s*\x28"; // if (
	static string whileRegex = @"while\s*\x28"; // while (
	static string bracesStart = @"\x28"; // (
	static string bracesEnd = @"\x29"; // )
	static string newScope = @"\x7B"; //{
	static string endOfScope = @"\x7D"; // }
	static string expressionEnd = @"\x3B"; // ;

	static void Main(string[] args)
	{
		foreach(string filename in args)
		{	
			Console.WriteLine(filename);
			using (StreamReader reader = new StreamReader(filename))
			{
				while(reader.Peek() >= 0)
				{
					rawInput.Add(reader.ReadLine());
				}
			}
		}

		Preprocess();
	}

	static void Preprocess()
	{
		DeleteSingleLineComments();
		DeleteDelimitedComments();
		
		foreach(string line in rawInput)
		{
			Console.WriteLine(line);
		}
	}

	static void DeleteSingleLineComments()
	{
		string oneLineComment = @"\x2F\x2F"; // // Something

		foreach(string line in rawInput)
		{
			string outputString = line;
			Match match = Regex.Match(outputString, oneLineComment);
			if(match.Success)
			{
				outputString = outputString.Remove(match.Index);
			}

			if(outputString.Length > 0)
			{
				output.Add(outputString);
			}
		}

		rawInput = output;
		output = new List<string>();
	}

	static void DeleteDelimitedComments()
	{
		string delimitedCommentStart = @"\x2F\x2A"; // /*
		string delimitedCommentEnd = @"\x2A\x2F"; // */
		string quoteSign = @"\x22";// "
		string quoteEscape = @"\x5C\x22"; // \"

		string savedString = "";

		foreach(string line in rawInput)
		{
			string outputString = savedString + line;

			MatchCollection commStarts = Regex.Matches(outputString, delimitedCommentStart);
			MatchCollection commEnds = Regex.Matches(outputString, delimitedCommentEnd);
			MatchCollection quoteSigns = Regex.Matches(outputString, quoteSign);
			MatchCollection quoteEscapes = Regex.Matches(outputString, quoteEscape);

			List<int> escapedQuotes = new List<int>();

			for(int i = 0; i < quoteSigns.Count; i++)
			{
				for(int j = 0; i < quoteEscapes.Count; j++)
				{
					if((quoteSigns[i].Index - 1) == (quoteEscapes[j].Index))
					{
						escapedQuotes.Add(quoteSigns[i].Index);
						break;
					}
				}
			}

			List<Symbol> symbols = new List<Symbol>();

			foreach(Match match in commStarts)
			{
				int index = match.Index;
				Symbol symbol = new Symbol(index, 2, "commStart");
				symbols.Add(symbol);
			}

			foreach(Match match in commEnds)
			{
				int index = match.Index;
				Symbol symbol = new Symbol(index, 2, "commEnds");
				symbols.Add(symbol);
			}

			foreach(Match match in quoteSigns)
			{
				int index = match.Index;
				if(!(escapedQuotes.Contains(index)))
				{
					Symbol symbol = new Symbol(index, 1, "quote");
					symbols.Add(symbol);
				}
			}

			SymbolComparer comparer = new SymbolComparer();
			symbols.Sort(comparer);

			string typeofBrace = "null";
			int braceStart = 0;
			int offset = 0;
			foreach(Symbol symbol in symbols)
			{
				string symbolType = symbol.Type;

				if(((symbolType == "quote") || (symbolType == "commStart")) && (typeofBrace == "null"))
				{
					typeofBrace = symbolType;
					braceStart = symbol.Index;
				}
				else if((typeofBrace == "commStart") && (symbolType == "commEnds"))
				{
					int symbolSize = symbol.Size;
					int braceEnd = symbol.Index;
					int count = braceEnd - braceStart + symbolSize;
					int start = braceStart - offset;
					outputString = outputString.Remove(start, count);
					offset = offset + count;
					typeofBrace = "null";
				}
				else if((typeofBrace == "quote") && (symbolType == "quote"))
				{
					typeofBrace = "null";
				}				
			}

			if(typeofBrace == "null")
			{
				savedString = "";
				output.Add(outputString);
			}
			else
			{
				savedString = outputString;
			}
		}

		rawInput = output;
		output = new List<string>();
	}
}